Api allowing to recognize objects on pictures

Technologies: Aws Lambda, Cloud9, S3 Bucket, Aws Rekognition, DynamoDB


Watch step1-step3 .png for screenshots.

1. Create blob by calling lambda:

POST https://rj0qxeo327.execute-api.us-west-1.amazonaws.com/dev/blobs

Body:
{
    "callback_url":"https://webhook.site/a4fd2e77-367f-4d26-a18b-fd859cd4afe5"
}

Response:
{
    "blob_id": "92df54",
    "callback_url": "https://webhook.site/a4fd2e77-367f-4d26-a18b-fd859cd4afe5",
    "upload_url": "https://s3.us-west-1.amazonaws.com/blobsbucket/92df54.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIATX6FHXMUN5M2TLUZ%2F20210920%2Fus-west-1%2Fs3%2Faws4_request&X-Amz-Date=20210920T122612Z&X-Amz-Expires=36000&X-Amz-SignedHeaders=host&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEH0aCXVzLXdlc3QtMSJIMEYCIQDPitIBjqeHs8BEcF18ce%2FdxEb9pVFwVyqzmTI9XOb0mAIhAPSkYa8GgMh%2FwGeMKoHpLNkZ0b%2FLcF2RuJ1ER6%2BmOJNhKq8CCNb%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEQARoMMjU3NTc0ODEyNDU2IgwjG4NBtWs2G8UXylwqgwJ3S57UAYn8v6U0FRKg8A5SAsM2VKUHmgScFaOwQStF1hlgStIxkOzzpXxTUi9j1%2B0J%2BjPDni6YMRMc5BFeEXglWAqCeRqbIUVFdOiPfO060j9kFG3p10QPyReHHw8K%2BLsFSzVaGQJdeaPd5G8TwF9kKqguUkcZkQ4x0sRj6QRYxSdSUAxZCqAsvwo5V%2BDTF9qayYzKem4Ud0BTQWwTHjwkaYZP%2FdCpklzXDBAVuGDPWFasNFWajC4Z7IZYJ40TxTDGvWdHKAtz%2BQpNUK%2FHKSkiT2moFu2IyVOmlZhi52L3lX26KgpoWgv36oq%2BS0WOXGR7sePkgMowmjbYftSJSyNudOyLMOP7oYoGOpkBUpN6lDJh0lMkyJIWor4CKPd8CCkLpO922SVh5fz%2BdxM7ysMXcfIa%2BKJKE896iEFMahMBoaRJ5bAV%2B%2BiLLgsZsEmkP8ZXSzlA3rfTfCo1cmLjk1tk%2FERHKDaIAoPwLYNBITozOMv6mmYF0FEdPzx3aBc58qiJVBkw2gcxf7a%2F7h4M9ETzGlpi5cNy3fQrV7ika6XTWHie4b3E&X-Amz-Signature=50a3f77c8f19ef30bd2f5281d74b9a5a0661dbef4a1793fa4bd5a0eaca642696",
    "hint": "Put .jpg binary file in Body",
    "check_url": "https://dgk6hix9v2.execute-api.us-west-1.amazonaws.com/dev/blobs/92df54"
}

2. Call "upload_url" PUT with binary file in Body.

Responce 200

3. Get result by GET "callback_url" (https://rj0qxeo327.execute-api.us-west-1.amazonaws.com/dev/blobs/92df54)

Response:
{
    "blob_id": "92df54",
    "status": "finished",
    "labels": [
        {
            "Name": "Bird",
            "Confidence": 99.2403335571289,
            "Instances": [
                {
                    "BoundingBox": {
                        "Width": 0.26799488067626953,
                        "Height": 0.35217466950416565,
                        "Left": 0.36037537455558777,
                        "Top": 0.33756405115127563
                    },
                    "Confidence": 99.2403335571289
                }
            ],
            "Parents": [
                {
                    "Name": "Animal"
                }
            ]
        },
        {
            "Name": "Animal",
            "Confidence": 99.2403335571289,
            "Instances": [],
            "Parents": []
        },
        {
            "Name": "Blackbird",
            "Confidence": 82.2827377319336,
            "Instances": [],
            "Parents": [
                {
                    "Name": "Bird"
                },
                {
                    "Name": "Animal"
                }
            ]
        },
        {
            "Name": "Agelaius",
            "Confidence": 82.2827377319336,
            "Instances": [],
            "Parents": [
                {
                    "Name": "Bird"
                },
                {
                    "Name": "Animal"
                }
            ]
        },
        {
            "Name": "Jay",
            "Confidence": 69.28691864013672,
            "Instances": [],
            "Parents": [
                {
                    "Name": "Bird"
                },
                {
                    "Name": "Animal"
                }
            ]
        },
        {
            "Name": "Silhouette",
            "Confidence": 67.31206512451172,
            "Instances": [],
            "Parents": []
        },
        {
            "Name": "Crow",
            "Confidence": 59.68186569213867,
            "Instances": [],
            "Parents": [
                {
                    "Name": "Bird"
                },
                {
                    "Name": "Animal"
                }
            ]
        }
    ]
}



