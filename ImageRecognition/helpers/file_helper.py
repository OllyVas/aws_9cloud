import uuid


def create_temp_file(size, file_content):
    random_index = str(uuid.uuid4().hex[:6])
    random_file_name = random_index + '.txt'
    with open(random_file_name, 'w') as f:
        f.write(str(file_content) * size)
    return random_index