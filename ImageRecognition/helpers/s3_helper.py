import boto3
import logging
from botocore.exceptions import ClientError
import base64
from botocore.client import Config
import os

S3_REGION = os.environ.get('BLOBS_REGION')
S3_BUCKET = os.environ.get('S3_BUCKET')
#S3_REGION = "us-west-1"
#S3_BUCKET = "blobsbucket"

def check_bucket():
    s3 = boto3.resource('s3', region_name=(S3_REGION))
    
    if (s3.Bucket(S3_BUCKET) in s3.buckets.all()):
        logging.info("exist")
        return True
    else:
        try:
            location = {'LocationConstraint': S3_REGION}
            s3.create_bucket(Bucket=S3_BUCKET, CreateBucketConfiguration=location)
            logging.info("created")
        except ClientError as err:
            logging.error("not created")
            logging.error(err)
        return False
    return True
    
def upload_file(file, object_name):
    s3 = boto3.resource('s3', region_name=(S3_REGION))
    try:
        response = s3.meta.client.upload_file(file, S3_BUCKET, object_name, ExtraArgs={"Metadata": {"metadata1":"ImageName","metadata2":"ImagePROPERTIES" ,"metadata3":"ImageCREATIONDATE"}}) #file name / bucket / object
        
        #s3.Object('mybucket', 'myfile.txt').put(Body='hello world', Metadata={'foo': 'bar'})
        logging.info(response)
    except ClientError as e:
        logging.error(e)
        return False
    return True
    
    
def put_file64(name, image):
    s3 = boto3.resource('s3', region_name=(S3_REGION))
    obj = s3.Object(S3_BUCKET, name)
    obj.put(Body=image)
    return True


def url_post(file):
    s3 = boto3.client('s3', 
        region_name=(S3_REGION), 
        config=Config(s3={'addressing_style': 'path'}, signature_version='s3v4'))
        
    return s3.generate_presigned_url(
        'put_object', 
        Params={'Bucket': S3_BUCKET, 'Key': file},#, 'ACL':'public-read',},
        ExpiresIn=36000)
        

def list_objects():
    s3 = boto3.resource('s3', region_name=(S3_REGION))
    resp = s3.meta.client.list_objects_v2(Bucket=S3_BUCKET)
    for obj in resp['Contents']:
        logging.info(obj['Key'])
    
    
def add_metadata(key, meta):
    s3 = boto3.resource('s3', region_name=(S3_REGION))
    s3_object = s3.Object(S3_BUCKET, key)
    s3_object.metadata.update(meta)
    s3_object.copy_from(CopySource={'Bucket':S3_BUCKET, 'Key':key}, Metadata=s3_object.metadata, MetadataDirective='REPLACE')
    
    
def get_metadata(key):
    s3 = boto3.resource('s3', region_name=(S3_REGION))
    s3_object = s3.Object(S3_BUCKET, key)
    return s3_object.metadata
 