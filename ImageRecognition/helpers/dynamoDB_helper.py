import os
import boto3
import logging
from botocore.exceptions import ClientError
import base64

DB_REGION = os.environ.get('BLOBS_REGION')
DB_TABLE = os.environ.get('DB_TABLE')
#DB_REGION = "us-west-1"
#DB_TABLE = "blobstable"

def create_table():
    dynamodb = boto3.client('dynamodb', region_name=(DB_REGION))
    existing_tables = dynamodb.list_tables()['TableNames']
    if DB_TABLE not in existing_tables:
        table = dynamodb.create_table(
            TableName=DB_TABLE,
            KeySchema=[
                {
                    'AttributeName': 'blob_id',
                    'KeyType': 'HASH'  # Partition key
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'blob_id',
                    'AttributeType': 'S'
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 10,
                'WriteCapacityUnits': 10
            }
        )

def add_bucket(file_id, status, labels, callback_url):
    dynamodb = boto3.resource('dynamodb', region_name=(DB_REGION))
    table = dynamodb.Table(DB_TABLE)
    
    try:
        table.put_item(
           Item={
                'blob_id': file_id,
                'blob_status': status,
                'labels': labels,
                'callback_url': callback_url
            }
        )
        logging.info("uploaded")
        return True
    except:
        return False
        
def get_bucket(file_id):
    dynamodb = boto3.resource('dynamodb', region_name=(DB_REGION))
    table = dynamodb.Table(DB_TABLE)
    
    try:
        response = table.get_item(Key={'blob_id': file_id})
        logging.info(response)
        return response['Item']
    except KeyError as e:
        raise e
        
def update_url(file_id, callback_url):
    dynamodb = boto3.resource('dynamodb', region_name=(DB_REGION))
    table = dynamodb.Table(DB_TABLE)
    
    response = table.update_item(
        Key={'blob_id': file_id},
        UpdateExpression="set callback_url = :c",
        ExpressionAttributeValues={
            ':c' : callback_url
        },
        ReturnValues="UPDATED_NEW"
    )
    logging.info(response)
    
def update_status(file_id, status):
    dynamodb = boto3.resource('dynamodb', region_name=(DB_REGION))
    table = dynamodb.Table(DB_TABLE)
    
    response = table.update_item(
        Key={'blob_id': file_id},
        UpdateExpression="set blob_status = :s",
        ExpressionAttributeValues={
            ':s': status
        },
        ReturnValues="UPDATED_NEW"
    )
    logging.info(response)

def update_labels(file_id, labels):
    dynamodb = boto3.resource('dynamodb', region_name=(DB_REGION))
    table = dynamodb.Table(DB_TABLE)
    
    response = table.update_item(
        Key={'blob_id': file_id},
        UpdateExpression="set labels = :l",
        ExpressionAttributeValues={
            ':l': labels
        },
        ReturnValues="UPDATED_NEW"
    )
    logging.info(response)