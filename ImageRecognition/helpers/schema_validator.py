import json
import logging
import jsonschema
from jsonschema import validate

schemas = {
    'post_blob' : {
            "callback_url" : {"type" : "string"},
            "required": ["callback_url"]
        },
}

def validate_correct_json(jsonData):
    try:
        json.loads(jsonData)
    except ValueError as err:
        logging.error(err)
        raise err
    return True
    
    
def validate_correct_schema(jsonData, jsonSchema):
        
    try:
        validate(instance=jsonData, schema=schemas[jsonSchema])
    except jsonschema.exceptions.ValidationError as err:
        logging.error(err)
        raise err
    return True