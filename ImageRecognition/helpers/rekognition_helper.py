import boto3
import logging
import os

BUCKET = os.environ.get('S3_BUCKET')
REGION = os.environ.get('BLOBS_REGION')

def detect_labels(photo):

    try:
        client=boto3.client('rekognition', region_name=(REGION))
        response = client.detect_labels(Image={'S3Object':{'Bucket':BUCKET,'Name':photo}},
            MaxLabels=10)
    except:
        return None
        
    print('Detected labels for ' + photo) 
    print()   
    for label in response['Labels']:
        print ("Label: " + label['Name'])
        print ("Confidence: " + str(label['Confidence']))
        print ("Instances:")
        for instance in label['Instances']:
            print ("  Bounding box")
            print ("    Top: " + str(instance['BoundingBox']['Top']))
            print ("    Left: " + str(instance['BoundingBox']['Left']))
            print ("    Width: " +  str(instance['BoundingBox']['Width']))
            print ("    Height: " +  str(instance['BoundingBox']['Height']))
            print ("  Confidence: " + str(instance['Confidence']))
            print()

        print ("Parents:")
        for parent in label['Parents']:
            print ("   " + parent['Name'])
        print ("----------")
        print ()
    return response['Labels']