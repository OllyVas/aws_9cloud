import json
import sys
import os
import boto3
import jsonpickle
import logging
import uuid
import base64
import time
import random
from string import ascii_lowercase
import helpers.s3_helper as s3_helper
from botocore.exceptions import ClientError
from helpers.schema_validator import validate_correct_schema
from helpers.file_helper import create_temp_file


_logger = logging.getLogger()
_logger.setLevel(logging.INFO)

def random_string(n):
    return ''.join(random.choice(ascii_lowercase) for i in range(n))


def blobs_post(event, context):
    _logger.info('## ENVIRONMENT VARIABLES\r' + jsonpickle.encode(dict(**os.environ)))
    _logger.info('## EVENT\r' + jsonpickle.encode(event))
    _logger.info('## CONTEXT\r' + jsonpickle.encode(context))
    
    validate_correct_schema(event, 'post_blob')
    
    s3_helper.check_bucket()
    
    file_name = str(uuid.uuid4().hex[:6])
    
    if (s3_helper.put_file64(file_name, "")) :
        print("file uploaded")
        s3_helper.add_metadata(file_name, {"status" : "empty", "labels":"", "callback_url" : json.loads(event["body"])["callback_url"]})
    #print("file created")
    #print(file_name)
    
    #if (s3_helper.upload_file(file_name + ".txt", file_name)) :
        #print("file uploaded")
        #url = s3_helper.get_post(file_name)
        #print(url)
    
    #os.remove(file_name + ".txt")
    #print("file removed")
    
    s3_helper.list_objects()
    
    
    
    body = {
        "blob_id": file_name,
        "callback_url": "https://webhook.site/",
        "upload_url": "https://dgk6hix9v2.execute-api.us-west-1.amazonaws.com/dev/blobs_upload/" + file_name,
        "hint": "Put binary file in Body",
        "check_url": ""
    }
    
    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }
    
    print(response)
    
    return response
    
def blobs_upload(event, context):
    print(event['pathParameters']['id'])
    file_id = event['pathParameters']['id']
    file_content = event['body']
    meta = s3_helper.get_metadata(file_id)
    if (s3_helper.put_file64(file_id, file_content)) :
        print("file uploaded")
        meta["status"] =  "in work"
        s3_helper.add_metadata(file_id, meta)
        #blobs_work({"Records" : {"0" : {"s3": {"object" : {"key" : file_id}}}}, "body" : ""}, "")
    
    body = {
        "message": "Uploaded successfully!"
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }
    print("end")
    return response
    

def blobs_check(event, context):
    print(event['pathParameters']['id'])
    blob_id = event['pathParameters']['id']
    meta = s3_helper.get_metadata(blob_id)
    
    body = {
        "blob_id": blob_id,
        "status" : meta["status"],
        "labels": json.loads("[" + meta["labels"] + "]")
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }
    return response
    
    
    
def blobs_work(event, context):
    print(event)
    file_id = event['Records']['0']['s3']['object']['key']
    time.sleep(6)
    meta = s3_helper.get_metadata(file_id)
    labels = []
    for x in range(3):
        new_label = {
                "label": "label_" + random_string(4),
                "confidence": random.randint(50, 100),
                "parents": [
                    random_string(4),
                    random_string(4)
                ]
            }
        labels.append(json.dumps(new_label))
        
    meta["labels"] += ','.join(labels)
    meta["status"] = "finished"
    s3_helper.add_metadata(file_id, meta)
    
    

def test(event, context):
    _logger.info('## ENVIRONMENT VARIABLES\r' + jsonpickle.encode(dict(**os.environ)))
    _logger.info('## EVENT\r' + jsonpickle.encode(event))
    _logger.info('## CONTEXT\r' + jsonpickle.encode(context))
    
    s3_helper.check_bucket()
    
    body = {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "input": event
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }
    return response

    # Use this code if you don't use the http event with the LAMBDA-PROXY
    # integration
    """
    return {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "event": event
    }
    """


if __name__ == "__main__":
    test('1', '2')
    
    req_post = '{"callback_url" : "text"}'
    load = json.loads(req_post)
    blobs_post(load, '')
    req_upload = {'pathParameters' : {"id" : "text"}, "body" : ""}
    #load = json.loads(req_upload)
    blobs_upload(req_upload, "")
    
    