import json
import sys
import os
import boto3
import jsonpickle
import logging
import uuid
import base64
import time
import random
import requests
import jsonschema
from string import ascii_lowercase
import helpers.s3_helper as s3_helper
import helpers.rekognition_helper as rek_helper
import helpers.dynamoDB_helper as dynamodb
from botocore.exceptions import ClientError
from helpers.schema_validator import validate_correct_schema
from helpers.schema_validator import validate_correct_json
from helpers.file_helper import create_temp_file


_logger = logging.getLogger()
_logger.setLevel(logging.INFO)

def random_string(n):
    return ''.join(random.choice(ascii_lowercase) for i in range(n))


def blobs_post(event, context):
    _logger.info('## ENVIRONMENT VARIABLES\r' + jsonpickle.encode(dict(**os.environ)))
    _logger.info('## EVENT\r' + jsonpickle.encode(event))
    _logger.info('## CONTEXT\r' + jsonpickle.encode(context))
    _logger.info('## BODY\r' + event["body"])
    
    try:
        validate_correct_schema(json.loads(event["body"]), 'post_blob')
    except (ValueError, jsonschema.exceptions.ValidationError) as err:
        body = {
            "Error": "Input callback_url."
        }
        response = {
            "statusCode": 404,
            "body": json.dumps(body)
        }
        logging.info(response)
        return response
        
    file_name = str(uuid.uuid4().hex[:6])
    callback = json.loads(event["body"])["callback_url"]
    
    if not dynamodb.add_bucket(file_name, "not uploaded", "", callback):
        body = {
            "error": "Adding to DB error."
        }
        response = {
            "statusCode": 404,
            "body": json.dumps(body)
        }
        _logger.info(response)
        return response
        
    upload_url = s3_helper.url_post(file_name)
    body = {
        "blob_id": file_name,
        "callback_url": callback,
        "upload_url": upload_url,
        "hint": "Put binary file in Body",
        "check_url": "https://dgk6hix9v2.execute-api.us-west-1.amazonaws.com/dev/blobs/" + file_name
    }
    
    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }
    
    _logger.info(response)
    return response
    
    
def blobs_check(event, context):
    _logger.info('## BUCKET ID\r' + event['pathParameters']['id'])
    blob_id = event['pathParameters']['id']
    
    try:
        bucket = dynamodb.get_bucket(blob_id)
        labels = json.loads(bucket["labels"]) if bucket["labels"] else ""
        body = {
            "blob_id": blob_id,
            "status" : bucket["blob_status"],
            "labels": labels
        }
        response = {
            "statusCode": 200,
            "body": json.dumps(body)
        }
        return response
        
    except (ClientError, KeyError) as e:
        _logger.info("Error")
        body = {
            "error": "Not found in DB error."
        }
        response = {
            "statusCode": 404,
            "body": json.dumps(body)
        }
        return response
    
    
def blobs_work(event, context):
    _logger.info('## EVENT \r' + jsonpickle.encode(event))
    file_name = event['Records'][0]['s3']['object']['key']
    _logger.info('## NAME \r' + file_name)
    
    file_id = file_name.split('.')[0]
    labels = rek_helper.detect_labels(file_name)
    if labels != None:
        _logger.info(labels)
    
        dynamodb.update_status(file_id, "finished")
        dynamodb.update_labels(file_id, json.dumps(labels))
    else:
        dynamodb.update_status(file_id, "Incorrect file uploaded!")
    
    
def blobs_callback(event, context):
    _logger.info('## ENVIRONMENT VARIABLES\r' + jsonpickle.encode(dict(**os.environ)))
    _logger.info('## EVENT\r' + jsonpickle.encode(event))
    _logger.info('## CONTEXT\r' + jsonpickle.encode(context))
    
    for update_record in event['Records']:
        if 'OldImage' in update_record['dynamodb'] and update_record['dynamodb']['NewImage']['labels']['S']:
            labels = json.loads(update_record['dynamodb']['NewImage']['labels']['S'].replace("\\", ""))
            body = {
                "blob_id": update_record['dynamodb']['NewImage']['blob_id']['S'],
                "status" : update_record['dynamodb']['NewImage']['blob_status']['S'],
                "labels": labels
            }
            req = requests.post(update_record['dynamodb']['NewImage']['callback_url']['S'], json.dumps(body))
            _logger.info(req)
    
    
def test(event, context):
    _logger.info('## ENVIRONMENT VARIABLES\r' + jsonpickle.encode(dict(**os.environ)))
    _logger.info('## EVENT\r' + jsonpickle.encode(event))
    _logger.info('## CONTEXT\r' + jsonpickle.encode(context))
    
    body = {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "input": event
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }
    return response


#if __name__ == "__main__":
    #test('1', '2')
    #s3_helper.check_bucket()
    
    #req_upload = {'pathParameters' : {"id" : "aa1a56.jpg"}, "body" :"""{"callback_url":"https://rj0qxeo327.execute-api.us-west-1.amazonaws.com/dev/test"}"""}
    #load = json.loads(req_upload)
    #print(blobs_post(req_upload, ''))
    #req_upload = {'pathParameters' : {"id" : "ad1b68"}, "body" : ""}
    #load = json.loads(req_upload)
    #blobs_upload(req_upload, "")
    #print(blobs_check(req_upload, ""))
    #blobs_work(req_upload, "")